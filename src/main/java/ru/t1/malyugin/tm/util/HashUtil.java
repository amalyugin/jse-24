package ru.t1.malyugin.tm.util;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "76534";

    @NotNull
    Integer ITERATION = 5455;

    @Nullable
    static String salt(@Nullable final String value) {
        if (StringUtils.isBlank(value)) return null;
        @Nullable String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    @Nullable
    static String md5(@Nullable final String value) {
        if (StringUtils.isBlank(value)) return null;
        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            @NotNull final byte[] array = messageDigest.digest(value.getBytes());
            @NotNull final StringBuilder stringBuilder = new StringBuilder();
            for (byte b : array) {
                stringBuilder.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return stringBuilder.toString();
        } catch (@NotNull final NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}